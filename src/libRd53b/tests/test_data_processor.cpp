#include "catch.hpp"

#include <iostream>

#include "AllProcessors.h"

#include "EventData.h"

#include "Rd53bCfg.h"

//#include "Rd53bEncodingTool.h"
#include "HitMapGenerator.h"
#include "Rd53bEncoder.h"

//#include "rd53b_test_stream.h"
//#include "rd53b_test_truth.h"

TEST_CASE("Rd53bDataProcessor", "[rd53b][data_processor]") {
  
  //Initialize the Rd53b generator/encoder, give it the random seed,
  //number of events per stream and generate desired number of events
  /*
  std::unique_ptr<Rd53bEncodingTool> encoder(new Rd53bEncodingTool());
  encoder->setSeed(Catch::rngSeed());
  encoder->setEventsPerStream(16);
  encoder->generate(28, 1e-2, 5, 1);

  //Retrieve the truth hits and the encoded words

  FrontEndData          truth    = encoder->getTruthData();
  */

  FrontEndData truth;

  std::unique_ptr<HitMapGenerator> generator(new HitMapGenerator());
  int nEvents = 18;
  int nEventsPerStream = 16;
  generator->setSeed(Catch::rngSeed());
  
  std::unique_ptr<Rd53bEncoder> encoder(new Rd53bEncoder());
  encoder->setEventsPerStream(nEventsPerStream);
  for (int evt = 0; evt < nEvents; evt++){
      generator->randomHitMap(1e-4);
      truth.events.push_back(generator->outTruth());
      if   (evt != nEvents - 1) encoder->addToStream(generator->outHits());
      else                      encoder->addToStream(generator->outHits(), true); //make sure the stream is ended with the last added event
  }
  
  std::vector<uint32_t> words = encoder->getWords();
  int nWords = words.size();
  
  std::shared_ptr<FeDataProcessor> proc = StdDict::getDataProcessor("RD53B");

  REQUIRE (proc);

  ClipBoard<RawDataContainer> rd_cp;
  ClipBoard<EventDataBase> em_cp;

  Rd53bCfg cfg;  

  proc->connect(&cfg, &rd_cp, &em_cp );

  proc->init();
  proc->run();

  RawDataPtr rd = std::make_shared<RawData>(0, nWords);
  uint32_t *buffer = rd->getBuf();
  buffer[nWords-1] = 0;

  std::copy(words.data(), words.data()+nWords, buffer);


  std::unique_ptr<RawDataContainer> rdc(new RawDataContainer(LoopStatus()));
  rdc->add(std::move(rd));
  rd_cp.pushData(std::move(rdc));

  rd_cp.finish();

  proc->join();

  REQUIRE (!em_cp.empty());

  auto data = em_cp.popData();
  FrontEndData &rawData = *(FrontEndData*)data.get();

  //REQUIRE (rawData.events.size() == truth_nEvents);
  REQUIRE (rawData.events.size() == truth.events.size());

  int truthNHits = 0;
  int rawNHits = 0;

  for (int ievt = 0; ievt < rawData.events.size(); ievt++){
	  for(int ihit = 0; ihit < rawData.events[ievt].hits.size(); ihit++){
		  REQUIRE(rawData.events[ievt].hits[ihit].col == truth.events[ievt].hits[ihit].col);
		  REQUIRE(rawData.events[ievt].hits[ihit].row == truth.events[ievt].hits[ihit].row);
		  REQUIRE(rawData.events[ievt].hits[ihit].tot == truth.events[ievt].hits[ihit].tot);
      rawNHits++;
	  }
    truthNHits += truth.events[ievt].nHits;
    
    if(ievt < rawData.events.size() - 1) {
      // All events must have equal hits, but for now we can sometimes drop the last hit..
      REQUIRE(rawNHits == truthNHits);
    }
    else {
      // Allow dropped hit at end of stream (TODO: fix in a stable way)
      REQUIRE(((rawNHits == truthNHits - 1) || (rawNHits == truthNHits)));
    }
  }

  // Require non-empty processed data clipboard
  REQUIRE (em_cp.empty());
}
